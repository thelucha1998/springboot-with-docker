#FROM openjdk:11
#ARG JAR_FILE=build/libs/*.jar
#COPY ${JAR_FILE} app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]


FROM openjdk:8-jre-alpine
RUN apk update && apk --no-cache add curl \
    && apk add --no-cache ttf-dejavu \
    && apk add --no-cache msttcorefonts-installer \
    && update-ms-fonts && fc-cache -f
RUN apk add --no-cache tzdata
ENV TZ=Asia/Ho_Chi_Minh

LABEL description=" San Pham cua Tap Doan Bao Viet" \
      maintainer="buingocbong@baoviet.com.vn "
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
#COPY ./build/libs/EClaimBackend.jar /deployment/EClaimBackend.jar
EXPOSE 8001
CMD ["java","-jar","-Xms3G","-Xmx3G","-Dserver.port=8001","app.jar"]

